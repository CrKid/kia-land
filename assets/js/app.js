'use strict';
$('input[type="phone"]').inputmask({ 'mask': '+7 (999) 999-99-99', showMaskOnHover: false });

var smCountSW = 6, lgLoopSW = false;
if ( window.innerWidth <= 768 ) smCountSW = 4;
if ( window.innerWidth <= 768 ) lgLoopSW = true;
if ( window.innerWidth <= 540 ) smCountSW = 1;

var swSM = new Swiper('.cars .swiper-sm', {
	slidesPerView: smCountSW,
	spaceBetween: 15,
	loop: false,
	navigation: {
		nextEl: '.swnav-sm_next',
		prevEl: '.swnav-sm_prev',
	},
});

var swLG = new Swiper('.cars .swiper-lg', {
	loop: lgLoopSW,
	navigation: {
		nextEl: '.swnav-lg_next',
		prevEl: '.swnav-lg_prev',
	},
	on: {
		slideChange: function () {
			
			swSM.slideTo( this.activeIndex );
			$('a[role="slide"]').removeClass('c-yared');
			$('a[role="slide"][data-index="'+this.activeIndex+'"]').addClass('c-yared');
			$('.footer .disclamer').hide();
			$('.footer .disclamer[data-car="'+this.activeIndex+'"]').show();
		},
	}
});


$('a[role="slide"]').click(function() {

    $('a[role="slide"]').removeClass('c-yared');
    $(this).addClass('c-yared');
	
	var i = $(this).data('index');
	swLG.slideTo(i);
	
	return false;
});

$('a[role="openForm"]').click( function() {
	
	$('.remodal[data-remodal-id="'+$(this).data('modal')+'"]').find('span#Car').text($(this).data('car'));
	$('.remodal[data-remodal-id="'+$(this).data('modal')+'"]').find('input[name="Car"]').val($(this).data('car'));
	if ( typeof $(this).data('target') != 'undefined' ) $('.remodal[data-remodal-id="'+$(this).data('modal')+'"]').find('span#'+$(this).data('target')).text($(this).data('value'));
});

$('a[role="toggleMenu"]').click( function() {
	
	$('.mob-menu').slideToggle(300);
	var attr = ( $(this).find('use').attr('xlink:href') == '#Icon-Burger' ) ? '#Icon-Close' : '#Icon-Burger';
	$(this).find('use').attr('xlink:href', attr);
});

$('[role="scrole"]').click(function() {

    $('html, body').animate({ scrollTop: $('div[data-scrole="' + $(this).data('scrole') + '"]').offset().top }, 300);
	return false;
});

$('[role="sendForm"]').click(function() {

    var JSApp = {};
    JSApp.SendData = {};

    JSApp.SendData.AppName = 'Lands';
    JSApp.SendData.EventCategory = 'Заполена форма';
    JSApp.SendData.Flag = true;

    var Form = $(this).parents('form');
    var success = $(Form).siblings('.alert-success');
    var error = $(Form).siblings('.alert-danger');
	var Event = $(Form).data('event');

    Form.find('input, select, textarea').each(function(i, e) {

        JSApp.SendData[$(e).attr('name')] = $(e).val();
        $(e).removeClass('is-invalid');

        if ($(e).attr('required')) {

            if (!$(e).val()) {

                JSApp.SendData.Flag = false;
                $(e).addClass('is-invalid');
            }
        }
    });

    if (JSApp.SendData.Flag) {

        $.ajax({
            type: "POST",
            url: '/kia/ajax/',
            data: JSApp.SendData,
            success: function(data) {

                var res = JSON.parse(data);

                if (res.status == 'success') {

                    $(Form).slideUp(300);
                    $(success).slideDown(300);
                    $(error).hide();
					
                    if (typeof Matomo != 'undefined') _paq.push(["trackEvent", 'Формы лэндинга', JSApp.SendData.Form, JSApp.SendData.EventCategory]);
					JSApp.Layer = {
						'event': 'FormSubmission',
						'eventCategory': Event,
						'eventAction': 'submit',
						'eventLabel': JSApp.SendData.Car || null
					};
					dataLayer.push( JSApp.Layer );


                    var CallTouchURL = 'https://api-node7.calltouch.ru/calls-service/RestAPI/requests/23541/register/';
                    CallTouchURL += '?subject=Формы лэндинга - ' + JSApp.SendData.Form;
                    CallTouchURL += '&sessionId=' + window.call_value_31;
                    CallTouchURL += '&fio=' + JSApp.SendData.Name;
                    CallTouchURL += '&phoneNumber=' + JSApp.SendData.Phone.replace(/[^\d;]/g, '');

                    $.get(CallTouchURL);

                } else {

                    $(error).slideDown(300);
                }
            },
            error: function() {

                $(error).slideDown(300);
            }
        });
		
		YApps.AppPushStat( JSApp.SendData );
    }

    return false;
});
window.TalkMeSetup = {
	domain: "kia.yug-avto.ru"
}




