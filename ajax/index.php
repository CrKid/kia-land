<?php
	//Load Composer's autoloader
	require __DIR__.'/../vendor/autoload.php';
	
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	
	if ( $_POST ) {
		
		$mail = new PHPMailer(true);
		
		try {
		
			$mail->setFrom('landing@kia.yug-avto.ru', 'Form Sender');
			
			//Recipients
			$mail->addAddress('aglushko@kia.yug-avto.ru', '');
			$mail->addAddress('anton.marchenko@kia.yug-avto.ru', '');
			$mail->addAddress('vera.golubeva@yug-avto.ru', '');
			$mail->addAddress('artem.artemev@kia.yug-avto.ru', '');
			$mail->addAddress('callcenter@adv.yug-avto.ru', '');

//			$mail->addAddress('anton.boreckiy@yug-avto.ru', '');
//			$mail->addAddress('boretscy@gmail.com', '');
			
			$mail->CharSet = 'UTF-8';
		
			//Content
			$mail->isHTML(true);
			$mail->Subject = 'Заявка с лэндинга Kia. Форма: '.$_POST['Form'];
			$mail->Body = '';
			
			if ( $_POST['Name'] ) $mail->Body .= '<strong>Имя</strong>: '. $_POST['Name'].'<br />';
			if ( $_POST['Phone'] ) $mail->Body .= '<strong>Телефон</strong>: '. $_POST['Phone'].'<br />';
			if ( $_POST['Car'] ) $mail->Body .= '<strong>Автомобиль</strong>: '. strtoupper($_POST['Car']).'<br />';
			if ( (int)$_POST['Offer'] ) $mail->Body .= '<strong>Предложение конкурентов</strong>: '. number_format((int)$_POST['Offer'], 0, '', ' ').' ₽';
			
		
			$mail->send();
			echo json_encode(['status'=>'success']);
		
		} catch (Exception $e) {
			
			echo json_encode(['status'=>'error']);
		}
	}

?>