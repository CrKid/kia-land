<?php include __DIR__.'./../include/_header.php'; ?>
      
      <div class="container-fluid">
        <div class="row banner dialog">
          <div class="container">
          </div>
        </div>
      </div>
      
      <div class="container">
        <div class="row">
          <div class="col-md-12" style="margin-top: 60px;">
            <h1 class="text-center">Мы готовы к диалогу! Начните со звонка!</h1>
            <div style="height: 60px;"></div>
            <P>Купить новый автомобиль стало еще выгоднее! Только до 31 августа Вы можете стать счастливым обладателем автомобиля KIA с максимальной выгодой!</P>
            <P>Не упустите свой шанс купить автомобиль на Ваших условиях! Торопитесь, количество автомобилей уменьшается с каждым звонком!</P>
            <?php
				$csvs = scandir(__DIR__.'/csv');
				unset( $csvs[0], $csvs[1] );
				
				foreach ( $csvs as $csv ) {
					
					$handle = fopen( __DIR__.'/csv/'.$csv, 'r');
					$arCSV = [];
					while ( ($line = fgetcsv($handle, 0, ";") ) !== FALSE) {
						$arCSV[] = $line;
					}
					fclose($handle);
					
					$model = $arCSV[0][0];
					
					?>
                    <a name="<?=$model?>"></a>
                    <h2><?=$model?></h2>
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th scope="col" style="width: 40%">Модель</th>
                          <th scope="col" style="width: 30%" class="text-center">Базовая цена</th>
                          <th style="width: 30%"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ( $arCSV as $k => $car) {
							if ( $k != 0 ) {
								?>
                                <tr>
                                  <td><?=$model?> <?=iconv('WINDOWS-1251', 'UTF-8', $car[0])?></td>
                                  <td class="text-center"><?=number_format((int)preg_replace("/[^0-9]/", '', $car[1]), 0, '', ' ')?> ₽</td>
                                  <td class="text-center"><a class="button" href="#reservationForm" role="reservation" data-car="<?=$model?> <?=iconv('WINDOWS-1251', 'UTF-8', $car[0])?>, <?=number_format((int)preg_replace("/[^0-9]/", '', $car[1]), 0, '', ' ')?> ₽">О цене договоримся!</a></td>
                                </tr>
                                <?php
							}
                        } ?>
                      </tbody>
                    </table>
                    <?php
				}
				
			?>
            
            
          </div>
        </div>
      </div>
      
      
      
<?php include __DIR__.'/../include/_footer.php'; ?>