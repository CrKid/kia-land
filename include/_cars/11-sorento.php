      			<div class="swiper-slide">
                  <div class="car-main">
                    <svg class="car-logo" xmlns="http://www.w3.org/2000/svg">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Car-Sorento"></use>
                    </svg>
                    <div class="slogan">Мужской подход</div>
                    <div class="img">
                      <img src="assets/images/cars/sorento.png" />
                      <div class="price">от <span>1 539 900</span> руб.</div>
                    </div>
                  </div>
                  <div class="car-tth">
                    <ul class="tth">
                      <li><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Petrol"></use></svg>По городу<span>7.4</span>л/100 км</li>
                      <li class="pt-3"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Speed"></use></svg>0-100 км/ч<span>10.5</span></li>
                      <li class="pt-3"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Engine"></use></svg>Бензиновый<br />дизельный</li>
                      <li class="pt-3"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Power"></use></svg>197 л.с.<br />3800 об.мин</li>
                    </ul>
                    <a class="button" href="#reservationForm" role="reservation" data-car="Sorento">Бронировать</a>
                  </div>
                </div>