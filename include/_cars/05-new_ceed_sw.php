      			<div class="swiper-slide">
                  <div class="car-main">
                    <svg class="car-logo" xmlns="http://www.w3.org/2000/svg">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Car-NewCeedSW"></use>
                    </svg>
                    <div class="slogan">Пространство с умом</div>
                    <div class="img">
                      <img src="assets/images/cars/new_ceed_sw.png" />
                      <div class="price">от <span>1 029 900</span> руб.</div>
                    </div>
                  </div>
                  <div class="car-tth">
                    <ul class="tth">
                      <li><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Petrol"></use></svg>По городу<span>7.7</span>л/100 км</li>
                      <li class="pt-3"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Speed"></use></svg>0-100 км/ч<span>9.4</span></li>
                      <li class="pt-1"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Engine"></use></svg>Бензиновый<span>1.4 DOHC CVVT (T-GDI)</span></li>
                      <li class="pt-3"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Power"></use></svg>140 л.с.<br />6000 об.мин</li>
                    </ul>
                    <a class="button" href="#reservationForm" role="reservation" data-car="Новый cee'd_sw">Бронировать</a>
                  </div>
                </div>