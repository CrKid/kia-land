      			<div class="swiper-slide">
                  <div class="car-main">
                    <svg class="car-logo" xmlns="http://www.w3.org/2000/svg">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Car-Soul"></use>
                    </svg>
                    <div class="slogan">Яркий городской кроссовер</div>
                    <div class="img">
                      <img src="assets/images/cars/soul.png" />
                      <div class="price">от <span>861 910</span> руб.</div>
                    </div>
                  </div>
                  <div class="car-tth">
                    <ul class="tth">
                      <li><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Petrol"></use></svg>По городу<span>8.7</span>л/100 км</li>
                      <li class="pt-3"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Speed"></use></svg>0-100 км/ч<span>10.2</span></li>
                      <li class="pt-1"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Engine"></use></svg>Бензиновый с<br />турбонаддувом и<br />непосредственным впрыском</li>
                      <li class="pt-3"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Power"></use></svg>204 л.с.<br />6000 об.мин</li>
                    </ul>
                    <a class="button" href="#reservationForm" role="reservation" data-car="Soul (2018г)">Бронировать</a>
                  </div>
                </div>