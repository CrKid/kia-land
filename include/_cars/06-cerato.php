      			<div class="swiper-slide">
                  <div class="car-main">
                    <svg class="car-logo" xmlns="http://www.w3.org/2000/svg">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Car-CeratoClassic"></use>
                    </svg>
                    <div class="slogan">Во власти уверенности и комфорта</div>
                    <div class="img">
                      <img src="assets/images/cars/cerato.png" />
                      <div class="price">от <span>904 900</span> руб.</div>
                    </div>
                  </div>
                  <div class="car-tth">
                    <ul class="tth">
                      <li><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Petrol"></use></svg>По городу<span>9,0</span>л/100 км</li>
                      <li class="pt-3"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Speed"></use></svg>0-100 км/ч<span>9.3</span></li>
                      <li class="pt-3"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Engine"></use></svg>Бензиновый</li>
                      <li class="pt-3"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Power"></use></svg>150 л.с.<br />6500 об.мин</li>
                    </ul>
                    <a class="button" href="#reservationForm" role="reservation" data-car="Cerato (2019г)">Бронировать</a>
                  </div>
                </div>