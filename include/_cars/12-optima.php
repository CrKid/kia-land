      			<div class="swiper-slide">
                  <div class="car-main">
                    <svg class="car-logo" xmlns="http://www.w3.org/2000/svg">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Car-Optima"></use>
                    </svg>
                    <div class="slogan">Летайте бизнес-классом</div>
                    <div class="img">
                      <img src="assets/images/cars/optima.png" />
                      <div class="price">от <span>1 169 900</span> руб.</div>
                    </div>
                  </div>
                  <div class="car-tth">
                    <ul class="tth">
                      <li><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Petrol"></use></svg>По городу<span>12.5</span>л/100 км</li>
                      <li class="pt-3"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Speed"></use></svg>0-100 км/ч<span>7.4</span></li>
                      <li class="pt-1"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Engine"></use></svg>Бензиновый<span>2.0 T-GDI (Theta-II)</span></li>
                      <li class="pt-3"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Power"></use></svg>245 л.с.<br />6000 об.мин</li>
                    </ul>
                    <a class="button" href="#reservationForm" role="reservation" data-car="Optima">Бронировать</a>
                  </div>
                </div>