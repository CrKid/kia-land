
      
      <div class="container-fluid map" data-scrole="map">
        <div class="row">
          <div class="col-md-12">
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa87eda2c6873471c67992923d4e3a1c98f068ac11c330ead39356a7aa7fb117f&amp;width=100%&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>
          </div>
          <div class="col-md-3">
            <svg xmlns="http://www.w3.org/2000/svg">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Logo-YA_Short"></use>
            </svg>
            <h6>Официальный дилер<br />KIA в Краснодаре</h6>
            <div class="contact">
              <svg xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Map"></use>
              </svg>
              пос. Яблоновский,<br />ул. Краснодарская, 1/2
            </div>
            <div class="contact">
              <svg xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Phone"></use>
              </svg>
              <span class="kia_call_phone_2"><a href="tel:+78612223333">+7 (861) 222-33-33</a></span>
            </div>
          </div>
        </div>
      </div>
      
      <div class="container">
        <div class="row footer">
          <div class="col-md-12 text-justify" style="font-size: 9.6px; line-height: normal;">
			<?php foreach ( $Conf['Cars'] as $k => $car ) { ?>
			  <div class="disclamer" <?=(($k==0)?'style="display: block;"':'')?> data-car="<?=$k?>">
			    <?=$car['description']?>
			  </div>
			<?php } ?>
          </div>
          <div class="col">© <?=date('Y')?> КИА Моторс Рус</div>
          <div class="col text-right"><?=date('Y')?> ООО «Авто Профи»</div>
          <?php /*
          <div class="col-md-6"><svg class="phone-icon" xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Phone"></use></svg> <span class="kia_call_phone_1"><a class="kia_call_phone_1" role="phone" href="callto:+78612315555">+7 (861) 231-55-55</a></span></div>
          */ ?>
        </div>
      </div>
    
    </main>
    
    <?php include $_SERVER['DOCUMENT_ROOT'].'/kia/include/_svg.php'; ?>
    <?php include $_SERVER['DOCUMENT_ROOT'].'/kia/include/_modals.php'; ?>
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/js/swiper.min.js"></script>
    
    <script src="/kia/assets/js/app.js?<?=md5_file($_SERVER['DOCUMENT_ROOT'].'/kia/assets/js/app.js');?>"></script>
    
     <?php include $_SERVER['DOCUMENT_ROOT'].'/kia/include/_scripts.php'; ?>
  </body>
</html>