<?php // header('Location: https://kia.yug-avto.ru/special/purchase/item114009930.php'); ?>
<!doctype html>
<html lang="ru">
  
  <head>
  	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-N38GCHD');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <title>Суперпредложение Kia</title>

    <!-- Bootstrap core CSS -->
    <?php /* <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" /> */ ?>
    <link href="/kia/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal-default-theme.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/css/swiper.min.css" />
    
    <link href="/kia/assets/css/fonts.css?<?=md5_file($_SERVER['DOCUMENT_ROOT'].'/kia/assets/css/fonts.css');?>" rel="stylesheet">
    <link href="/kia/assets/css/app.css?<?=md5_file($_SERVER['DOCUMENT_ROOT'].'/kia/assets/css/app.css');?>" rel="stylesheet">
    <link href="/kia/assets/css/media.css?<?=md5_file($_SERVER['DOCUMENT_ROOT'].'/kia/assets/css/media.css');?>" rel="stylesheet">
    
    <link rel="apple-touch-icon" sizes="57x57" href="/kia/assets/images/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/kia/assets/images/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/kia/assets/images/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/kia/assets/images/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/kia/assets/images/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/kia/assets/images/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/kia/assets/images/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/kia/assets/images/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/kia/assets/images/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/kia/assets/images/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/kia/assets/images/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/kia/assets/images/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/kia/assets/images/fav/favicon-16x16.png">
    <link rel="manifest" href="/kia/assets/images/fav/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/kia/assets/images/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <meta name="yandex-verification" content="65e878d42cf0b453" />

  </head>

  <body>

    <?php include $_SERVER['DOCUMENT_ROOT'].'/kia/include/_counters.php'; ?>
    
    <main role="main">
    
      <div class="top-row py-3 py-md-5">
        <div class="container">
          <div class="row pc">
            <div class="col-md-2 brd-right">
              <a href="https://promo.yug-avto.ru/kia/">
                <svg class="logo-kia" xmlns="http://www.w3.org/2000/svg">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Logo-Kia"></use>
                </svg>
              </a>
            </div>
            <div class="col-md-4 top-text brd-right">Юг-Авто официальный дилер KIA<br />пос. Яблоновский, ул. Краснодарская, 1/2</div>
            <div class="col-md-4 text-center brd-right">
              <svg class="phone-icon" xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-PhoneO"></use></svg> 
              <span class="kia_call_phone_1"><a role="phone" href="callto:+78612315555">+7 (861) 231-55-55</a></span>
            </div>
            <div class="col-md-2 text-right">
              <a href="https://promo.yug-avto.ru/kia/">
                <svg class="logo-ya" xmlns="http://www.w3.org/2000/svg">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Logo-YA_Short"></use>
                </svg>
              </a>
            </div>
          </div>
          <div class="row mob">
            <div class="col-3 border-right border-dark">
              <a href="https://promo.yug-avto.ru/kia/">
                <svg class="logo-kia" xmlns="http://www.w3.org/2000/svg">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Logo-Kia"></use>
                </svg>
              </a>
            </div>
            <div class="col-5">
              <a href="https://promo.yug-avto.ru/kia/">
                <svg class="logo-ya" xmlns="http://www.w3.org/2000/svg">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Logo-YA_Short"></use>
                </svg>
              </a>
            </div>
            <div class="col-2 text-right">
              <span class="mob_kia_call_phone_1_1"><a href="callto:+78612315555">
                <svg xmlns="http://www.w3.org/2000/svg" style="fill: var(--yared);">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-PhoneO"></use>
                </svg>
              </a></span>
            </div>
            <div class="col-2 text-right">
              <a href="#" role="toggleMenu">
                <svg xmlns="http://www.w3.org/2000/svg">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Burger"></use>
                </svg>
              </a>
            </div>
          </div>
        </div>
      </div>
      
      <div class="container mob-menu bg-yablack">
        <div class="row py-3 bb-yawhite">
          <div class="col-2 pt-1"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-PhoneO"></use></svg></div>
          <div class="col-10"><span class="kia_call_phone_1"><a role="phone" href="callto:+78612315555">+7 (861) 231-55-55</a></span></div>
        </div>
        <div class="row py-3 bb-yawhite">
          <div class="col-2"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Search"></use></svg></div>
          <div class="col-10 pt-1"><a href="#" role="scrole" data-scrole="models">Выбрать автомобиль</a></div>
        </div>
        <div class="row py-3 bb-yawhite">
          <div class="col-2"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-Competitor"></use></svg></div>
          <div class="col-10 pt-1"><a href="#" role="scrole" data-scrole="offer">Предложение от конкурентов</a></div>
        </div>
        <div class="row py-3 bb-yawhite">
          <div class="col-2"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-MapO"></use></svg></div>
          <div class="col-10 pt-1"><a href="#" role="scrole" data-scrole="map">Схема проезда в KIA Юг-Авто</a></div>
        </div>
      </div>
      