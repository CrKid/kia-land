<div style="background: url(assets/images/cars_bg-all.jpg) repeat;" data-scrole="models">
  <div class="cars">
    <div class="container">
      
      <div class="row py-3 py-sm-5 pc">
        <div class="col-2 col-sm-1 sw-nav swnav-sm swnav-sm_prev text-center">
          <svg xmlns="http://www.w3.org/2000/svg">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-ArrowSM-L"></use>
          </svg>
        </div>
        <div class="col-8 col-sm-10">
          <div class="swiper-sm">
            <div class="swiper-wrapper">
              <?php foreach ( $Conf['Cars'] as $k => $car ) { ?>
              <div class="swiper-slide">
              	<a href="#" class="<?=(($k==0)?'c-yared':'')?>" role="slide" data-car="<?=$car['key']?>" data-index="<?=$k?>">• <?=$car['name']?></a>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="col-2 col-sm-1 sw-nav swnav-sm swnav-sm_next text-center">
          <svg xmlns="http://www.w3.org/2000/svg">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-ArrowSM-R"></use>
          </svg>
        </div>
      </div>
      
      <div class="row py-1 py-sm-3">
        <div class="col-2 col-sm-1 d-flex justify-content-center flex-wrap align-content-center sw-nav swnav-lg swnav-lg_prev">
          <svg xmlns="http://www.w3.org/2000/svg">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-ArrowLG-L"></use>
          </svg>
        </div>
        <div class="col-8 col-sm-10">
          <div class="swiper-lg">
            <div class="swiper-wrapper">
              <?php foreach ( $Conf['Cars'] as $k => $car ) { ?>
              <div class="swiper-slide" data-car="<?=$car['key']?>" data-index="<?=$k?>">
                <svg xmlns="http://www.w3.org/2000/svg">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Car-<?=$car['icon']?>"></use>
                </svg>
                <span class="pl-3 pc">
                  <?php if ( $car['discount'] ) { ?>
                  Выгода до <?=number_format($car['discount'], 0, '', ' ')?> ₽ <sup>*</sup><br />
                  <?php } ?>
                  <?php if ( $car['credit'] ) { ?>
                  Кредит от <?=number_format($car['credit'], 0, '', ' ')?> ₽/мес <sup>**</sup>
                  <?php } ?>
                </span>
                <div class="buttons pc">
                  <?php if ( $car['credit'] ) { ?>
                  <a class="but but-red but-ppg-car text-center text-uppercase" href="#formCredit" data-car="<?=$car['name']?>" role="openForm" data-modal="formCredit" data-target="Credit" data-value="<?=number_format($car['credit'], 0, '', ' ')?>">
                    В кредит от <?=number_format($car['credit'], 0, '', ' ')?> ₽/мес
                  </a>
                  <?php } ?>
                  <a class="but but-red but-ppg-car text-center mt-2 text-uppercase" href="#formTradein" role="openForm" data-modal="formTradein" data-car="<?=$car['name']?>">Обменяй на новую</a>
                  <a class="but but-red but-ppg-car text-center mt-2 text-uppercase" href="#formOffer" role="openForm" data-modal="formOffer" data-car="<?=$car['name']?>">Получи особые условия</a>
                </div>
                <span class="pl-3 mob">
                  <?php if ( $car['discount'] ) { ?>
                  Выгода до <?=number_format($car['discount'], 0, '', ' ')?> р <sup>*</sup><br />
                  <?php } ?>
                  <?php if ( $car['credit'] ) { ?>
                  Кредит от <?=number_format($car['credit'], 0, '', ' ')?> р/мес <sup>**</sup>
                  <?php } ?>
                </span>
                <div class="buttons mob">
                  <?php if ( $car['credit'] ) { ?>
                  <a class="but but-red but-ppg-car text-center text-uppercase" href="#formCredit" data-car="<?=$car['name']?>" role="openForm" data-modal="formCredit" data-target="Credit" data-value="<?=number_format($car['credit'], 0, '', ' ')?>">
                    Кредит от <?=number_format($car['credit'], 0, '', ' ')?> р/мес
                  </a>
                  <?php } ?>
                  <a class="but but-red but-ppg-car text-center mt-2 text-uppercase" href="#formTradein" role="openForm" data-modal="formTradein" data-car="<?=$car['name']?>">Обменяй на новую</a>
                  <a class="but but-red but-ppg-car text-center mt-2 text-uppercase" href="#formOffer" role="openForm" data-modal="formOffer" data-car="<?=$car['name']?>">Особые условия</a>
                </div>
                <img src="assets/images/cars/<?=$car['img']?>" />
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="col-2 col-sm-1 d-flex justify-content-center flex-wrap align-content-center sw-nav swnav-lg swnav-lg_next">
          <svg xmlns="http://www.w3.org/2000/svg">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-ArrowLG-R"></use>
          </svg>
        </div>
      </div>
      
    </div>
  </div>
</div>