<div class="alert alert-dismissible alert-success">
  Спасибо за Вашу заявку! Мы свяжемся с Вами в ближайшее время.
</div>
<div class="alert alert-dismissible alert-danger">
  Ой, что-то пошло не так. Повторите попытку позднее.
</div>