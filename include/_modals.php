<div class="remodal text-left" data-remodal-id="formCredit">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="h3 text-center text-uppercase">
    Кредит от <span id="Credit"></span> ₽/мес<br />
    на <strong>KIA <span id="Car"></span></strong>
  </div>
  <hr class="dotted my-3" />
  <form id="reservationForm" data-event="credit">
    <input type="hidden" name="Form" value="Получить кредит" />
    <input type="hidden" name="Car" value="" />
    <fieldset style="width: calc(100% - 60px); margin: 0 auto;">
      <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">Имя *</label>
        <input type="text" class="form-control form-control-lg bg-yalightgray" name="Name" required />
      </div>
      <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">Телефон *</label>
        <input type="phone" class="form-control form-control-lg bg-yalightgray" name="Phone" placeholder="+7 (___) ___-__-__" required />
      </div>
    </fieldset>
    <fieldset>
      <a class="but but-red but-ppg text-center mt-3" role="sendForm" href="#">Отправить</a>
      <?php include __DIR__.'/_form.personal.php'; ?>
    </fieldset>
  </form>
  <?php include __DIR__.'/_form.results.php'; ?>
</div>

<div class="remodal text-left" data-remodal-id="formTradein">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="h3 text-center text-uppercase">
    Заявка на трейд ин<br />
    на <strong>KIA <span id="Car"></span></strong>
  </div>
  <hr class="dotted my-3" />
  <form id="tradeinForm" data-event="sell-car">
    <input type="hidden" name="Form" value="Заявка на трейд ин" />
    <input type="hidden" name="Car" value="" />
    <fieldset style="width: calc(100% - 60px); margin: 0 auto;">
      <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">Имя *</label>
        <input type="text" class="form-control form-control-lg bg-yalightgray" name="Name" required />
      </div>
      <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">Телефон *</label>
        <input type="phone" class="form-control form-control-lg bg-yalightgray" name="Phone" placeholder="+7 (___) ___-__-__" required />
      </div>
    </fieldset>
    <fieldset>
      <a class="but but-red but-ppg text-center mt-3" role="sendForm" href="#">Отправить</a>
      <?php include __DIR__.'/_form.personal.php'; ?>
    </fieldset>
  </form>
  <?php include __DIR__.'/_form.results.php'; ?>
</div>

<div class="remodal text-left" data-remodal-id="formOffer">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="h3 text-center text-uppercase">
    Особое предложение<br />
    на <strong>KIA <span id="Car"></span></strong>
  </div>
  <hr class="dotted my-3" />
  <form id="offerForm" data-event="special">
    <input type="hidden" name="Form" value="Особое предложение" />
    <input type="hidden" name="Car" value="" />
    <fieldset style="width: calc(100% - 60px); margin: 0 auto;">
      <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">Имя *</label>
        <input type="text" class="form-control form-control-lg bg-yalightgray" name="Name" required />
      </div>
      <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">Телефон *</label>
        <input type="phone" class="form-control form-control-lg bg-yalightgray" name="Phone" placeholder="+7 (___) ___-__-__" required />
      </div>
    </fieldset>
    <fieldset>
      <a class="but but-red but-ppg text-center mt-3" role="sendForm" href="#">Отправить</a>
      <?php include __DIR__.'/_form.personal.php'; ?>
    </fieldset>
  </form>
  <?php include __DIR__.'/_form.results.php'; ?>
</div>