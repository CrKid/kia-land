<?php include __DIR__.'/vendor/Config.php'; ?>
<?php include __DIR__.'/include/_header.php'; ?>
      
<div class="banner">
  <div class="container">
    <div class="row justify-content-end pc">
      <div class="col-5 p-5 bg-yawhite shadow">
        <h3 class="text-center text-uppercase">Узнай свою выгоду!</h3>
        <hr class="dotted my-3" />
        <form id="reservationForm" data-event="special">
          <input type="hidden" name="Form" value="Узнай свою выгоду!" />
          <fieldset style="width: calc(100% - 60px); margin: 0 auto;">
            <div class="form-group">
              <label class="col-form-label col-form-label-lg" for="inputLarge">Имя *</label>
              <input type="text" class="form-control form-control-lg bg-yalightgray" name="Name" required />
            </div>
            <div class="form-group">
              <label class="col-form-label col-form-label-lg" for="inputLarge">Телефон *</label>
              <input type="phone" class="form-control form-control-lg bg-yalightgray" name="Phone" placeholder="+7 (___) ___-__-__" required />
            </div>
          </fieldset>
          <fieldset>
            <a class="but but-red but-ppg text-center mt-3" role="sendForm" href="#">Отправить</a>
            <?php include __DIR__.'/include/_form.personal.php'; ?>
          </fieldset>
        </form>
        <?php include __DIR__.'/include/_form.results.php'; ?>
      </div>
    </div>
    
  </div>
</div>

<div class="container pc">
  <div class="row pt-5">
    <div class="col-md-12 text-uppercase">
      <h1>Преимущества kia в юг-авто</h1>
    </div>
  </div>
  <div class="row py-5 futures text-uppercase pc">
    <?php foreach ( $Conf['Futures'] as $f ) { ?>
    <div class="col-2 col-lg-1 col-sm-3 py-4 d-flex flex-wrap align-content-center">
      <span class="shadow-sm">
        <svg xmlns="http://www.w3.org/2000/svg">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-<?=$f['icon']?>"></use>
        </svg>
      </span>
    </div>
    <div class="col-4 col-lg-3 py-4 pl-lg-5 d-flex flex-wrap align-content-center">
      <?=$f['text']?>
    </div>
    <?php } ?>
  </div>
  <div class="row py-5 futures text-uppercase mob">
    <?php foreach ( $Conf['Futures'] as $f ) { ?>
    <div class="col-sm-2 col-4 py-4 d-flex flex-wrap align-content-center">
      <span>
        <svg xmlns="http://www.w3.org/2000/svg">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Icon-<?=$f['icon']?>"></use>
        </svg>
      </span>
    </div>
    <div class="col-sm-4 col-8 py-4 pl-lg-5 d-flex flex-wrap align-content-center">
      <?=$f['text']?>
    </div>
    <?php } ?>
  </div>
</div>

<?php include __DIR__.'/include/_cars.php'; ?>

<div class="offer py-5" data-scrole="offer">
  <div class="container position-relative bg-yawhite">
    <div class="row pc">
      <div class="col-md-5 p-5 bg-white shadow ">
        <form id="reservationForm" data-event="car">
          <input type="hidden" name="Form" value="Предложение конкурентов" />
          <fieldset style="width: calc(100% - 60px); margin: 0 auto;">
            <div class="form-group">
              <label class="col-form-label col-form-label-lg" for="inputLarge">Модель *</label>
              <select class="form-control form-control-lg bg-yalightgray">
                <option disabled selected>Выбрать..</option>
                <?php foreach ( $Conf['Cars'] as $car ) { ?>
                <option value="<?=$car['name']?>">KIA <?=$car['name']?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label class="col-form-label col-form-label-lg" for="inputLarge">Предложение конкурентов *</label>
              <input type="number" min="0" class="form-control form-control-lg bg-yalightgray" name="Offer" required />
            </div>
            <div class="form-group">
              <label class="col-form-label col-form-label-lg" for="inputLarge">Телефон *</label>
              <input type="phone" class="form-control form-control-lg bg-yalightgray" name="Phone" placeholder="+7 (___) ___-__-__" required />
            </div>
          </fieldset>
          <fieldset>
            <a class="but but-red but-ppg text-center mt-3" role="sendForm" href="#">Получить предложение</a>
            <?php include __DIR__.'/include/_form.personal.php'; ?>
          </fieldset>
        </form>
        <?php include __DIR__.'/include/_form.results.php'; ?>
      </div>
      <div class="col-md-1"></div>
      <div class="col-md-6">
        <div class="h1 text-uppercase c-yagray">Есть предложение от конкурентов?</div>
        <div class="h3 mt-5 c-yagray">&larr; просто укажи его</div>
      </div>
    </div>
    
    <div class="row mob">
      <div class="col-sm-2"></div>
      <div class="col-sm-8">
        <div class="h1 text-uppercase c-yagray">Есть предложение от конкурентов?</div>
        <div class="h3 mt-lg-5 c-yagray">просто укажи его &darr;</div>
      </div>
      <div class="col-sm-2"></div>
      <div class="col-sm-2"></div>
      <div class="col-sm-8 py-3 px-5 p-lg-5 bg-white shadow ">
        <form id="reservationForm" data-event="car">
          <input type="hidden" name="Form" value="Предложение конкурентов" />
          <fieldset style="width: calc(100% - 60px); margin: 0 auto;">
            <div class="form-group">
              <label class="col-form-label col-form-label-lg" for="inputLarge">Модель *</label>
              <select class="form-control form-control-lg bg-yalightgray" required>
                <option disabled selected>Выбрать..</option>
                <?php foreach ( $Conf['Cars'] as $car ) { ?>
                <option value="<?=$car['name']?>">KIA <?=$car['name']?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label class="col-form-label col-form-label-lg" for="inputLarge">Предложение конкурентов *</label>
              <input type="number" class="form-control form-control-lg bg-yalightgray" name="Offer" required />
            </div>
            <div class="form-group">
              <label class="col-form-label col-form-label-lg" for="inputLarge">Телефон *</label>
              <input type="phone" class="form-control form-control-lg bg-yalightgray" name="Phone" placeholder="+7 (___) ___-__-__" required />
            </div>
          </fieldset>
          <fieldset>
            <a class="but but-red but-ppg text-center mt-3" role="sendForm" href="#">Отправить</a>
            <?php include __DIR__.'/include/_form.personal.php'; ?>
          </fieldset>
        </form>
        <?php include __DIR__.'/include/_form.results.php'; ?>
      </div>
    </div>
    <div class="col-sm-2 pc"></div>
    <img class="pc" src="assets/images/offer-car.png" />
  </div>
</div>

<?php include __DIR__.'/include/_footer.php'; ?>