<?php include __DIR__.'./../include/_header.php'; ?>
      
      <div class="container-fluid">
        <div class="row banner turkey banner-turkey">
          <div class="container">
            <h1></h1>
          </div>
        </div>
      </div>
      
      <div class="container">
        <div class="row">
          <div class="col-md-12" style="margin-top: 60px;">
            <h1 class="text-center">Поездка в Турцию в подарок!</h1>
            <div style="height: 60px;"></div>
            <P>Продли себе лето вместе с КИА Юг-Авто! Только в октябре при покупке ЛЮБОГО автомобиля КИА мы дарим вам Сертификат на недельный отдых в Турции в отеле 5 звезд! *</P>
            <ul>
              <li>Проживание в отеле 5*</li>
              <li>Продолжительность тура 8 дней / 7 ночей</li>
              <li>Ежедневная развлекательная программа</li>
              <li>Обзорная экскурсия</li>
              <li>Экскурсия в Памуккале</li>
            </ul>
            <p>Вас ждет незабываемый отдых на побережье Средиземного моря!</p>
            <p>Проживание и развлекательная программа тура БЕСПЛАТНО для 2-х персон! **</p>

            <hr />
            <div class="text-left">
              <h2>Хочу в отпуск!</h2>
              <p>* - поля, обязательные для заполнения</p>
              <form id="travelForm">
                <input type="hidden" name="Form" value="Форма Хочу в отпуск" />
                <fieldset>
                  <div class="form-group">
                    <input type="text" class="form-control" name="Name" placeholder="Имя *">
                  </div>
                  <div class="form-group">
                    <input type="phone" class="form-control" name="Phone" placeholder="Телефон *">
                  </div>
                  <a class="button" role="sendForm" href="#">Отправить</a>
                  <small class="form-text text-muted">Отправляя заявку Вы соглашаетесь на <a href="/kia/rules/">обработку персональных данных</a></small>
                </fieldset>
              </form>
              <div class="alert alert-dismissible alert-success">
                Спасибо за Вашу заявку! Мы свяжемся с Вами в ближайшее время.
              </div>
              <div class="alert alert-dismissible alert-danger">
                Ой, что-то пошло не так. Повторите попытку позднее.
              </div>
            </div>
            <hr />

            <p>Подробности в Юг-Авто по телефону: <span class="kia_call_phone_1"><a role="phone" href="callto:+78612315555">+7 (861) 231-55-55</a></span></p>
          </div>
        </div>
      </div>

<?php $disclamer = '* Предложение ограничено, действительно до 31.10.2018 г., не оферта.<br /><br />** Авиаперелет оплачивается самостоятельно'; ?>
      
<?php include __DIR__.'/../include/_footer.php'; ?>